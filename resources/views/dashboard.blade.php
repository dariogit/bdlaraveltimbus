
<x-app-layout>
    <div class="wrapper">
        <div
          class="sidebar"
          data-color="orange"
          data-background-color="white"
          data-image=""
        ><br><p></p>
          <div class="logo" id="text-center">
             <a href="./index.html" class="simple-text logo-normal ">
              <img src="images/yonier.jpg" alt="usuario" class="img-fluid d-blok mx-auto" />
             </a>
          </div>

          <div class="sidebar-wrapper">
            <ul class="nav">

              <li class="nav-item">
                <a class="nav-link" href="{{ route('dashboard') }}">
                     {{-- {{route('dashboard')}} --}}
                  <img class="mr-2" src="../img/ic_view_quilt_24px.png" />
                  Dashboard
                </a>
              </li>

              <li class="nav-item">
                <a class="nav-link" href="{{ route('perfil') }}">
                    {{-- {{route('perfilEstudiante')}} --}}
                  <img class="mr-2" src="../img/Group 1380.png" />
                  Perfil
                </a>
              </li>

              <li class="nav-item">
                <a class="nav-link" href="{{ route('rutaEstudiante') }}">
                  <img class="mr-2" src="../bootstrap-icons/truck.svg"/>
                  Ruta
                </a>
              </li>

              <li class="nav-item">
                <a class="nav-link" href="{{ route('institucionEstudiante') }}">
                  <img class="mr-2" src="../img/Group 1382.png" />
                  Institucion
                </a>
              </li>
              <li class="nav-item">
                <a class="nav-link" href="{{ route('pagoTransporte') }}">
                  <img class="mr-2" src="../bootstrap-icons/cash-coin.svg" />
                  Pago Transporte
                </a>
              </li>


            </ul>
          </div>
        </div>
        <div class="main-panel">

          <div class="content">
            <div class="container-fluid">
              <div class="row">


                <article class="d-flex">
                  <div class="card p-3 mb-2 bg-warning text-dark" style="width: 18rem;">
                    <img src="../images/conductor.png" class="card-img-top" alt="...">
                    <div class="card-body">
                      <h5 class="card-title">Card Conductor</h5>
                      <p class="card-text">Some quick example text to build on the card title and make up the bulk of the card's
                        content.</p>
                      <!-- <a href="#" class="btn btn-primary">Go somewhere</a> -->


                    </div>
                  </div><br>

                  <div class="card p-3 mb-2 bg-info text-dark"" style=" width: 18rem;">
                    <img src="../images/institucion.jpg" class="card-img-top" alt="...">
                    <div class="card-body">
                      <h5 class="card-title">Card Institucion</h5>
                      <p class="card-text">Some quick example text to build on the card title and make up the bulk of the card's
                        content.</p>
                      <!-- <a href="#" class="btn btn-primary">Go somewhere</a> -->






                    </div>
                  </div><br>

                  <div class="card p-3 mb-2 bg-secondary text-dark" style="width: 18rem;">
                    <img src="../images/empresa.png" class="card-img-top" alt="...">
                    <div class="card-body">
                      <h5 class="card-title">Card Empresa</h5>
                      <p class="card-text">Some quick example text to build on the card title and make up the bulk of the card's
                        content.</p>
                      <!-- <a href="#" class="btn btn-primary">Go somewhere</a> -->





                    </div>
                  </div><br>

                  <div class="card p-3 mb-2 bg-warning text-dark" style="width: 18rem;">
                    <img src="../images/escolar4.jpg" class="card-img-top" alt="...">
                    <div class="card-body">
                      <h5 class="card-title">Card Estudiantes</h5>
                      <p class="card-text">Some quick example text to build on the card title and make up the bulk of the card's
                        content.</p>
                      <!-- <a href="#" class="btn btn-primary">Go somewhere</a> -->





                    </div>
                  </div><br>

                </article>

              </div>
              <div class="row">


                    </div>
                  </div>
                </div>

              </div>

            </div>
          </div>
        </div>
      </div>
</x-app-layout>
