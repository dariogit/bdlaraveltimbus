<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Institucion</title>
</head>
<body>
    @extends('layouts.plantilla') <br><p></p>

    @extends('layouts.navigation2')




    @section('contenidoPrincipal')
    <div class="content">
        <div class="container-fluid"><br>

<h3> Editar los Datos de la Institucion  {{$institucion->nombre}}</h3>

<form action="{{route('inst.Update', $institucion)}} " method="POST">
  @csrf
  @method('PUT')
  <label for="">nombre</label><br>
  <input type="text" name="nombre"   placeholder="nombre" value="{{$institucion->nombre}}" class="form-controll-lg">
  <br>
  <label for="">telefono</label><br>
  <input type="number" name="telefono"   placeholder="telefono" value="{{$institucion->telefono}}" class="form-controll mb-2">
  <br>
  <label for="">direccion</label><br>
  <input type="text" name="direccion"   placeholder="direccion" value="{{$institucion->direccion}}" class="form-controll mb-2">
  <br>
  <label for="">correo</label><br>
  <input type="email" name="correo"   placeholder="correo" value="{{$institucion->correo}}" class="form-controll mb-2">
  <br>

  {{-- <label for="">Direccion</label><br>
  <input type="text" name="direccion"   placeholder="Direccion" value="{{$paciente->direccion}}" class="form-controll mb-2">
  <br>
  <label for="">Telefono</label><br>
  <input type="number" name="telefono"   placeholder="Telefono" value="{{$paciente->telefono}}" class="form-controll mb-2">
  <br>
  <label for="">Contraseña</label><br>
  <input type="password" name="password"   placeholder="Password" value="{{$paciente->password}}" class="form-controll mb-2"> --}}

  <button class="btn btn-warning"  type="submit" >Guardar</button>

  <button href="{{route('institucionesVinculadas')}}" class="btn btn-danger"  type="submit" >Volver</button>

{{-- <li class="nav-item">
    <a class="nav-link" href="{{route('institucionesVinculadas')}}" id="testimonio">Volver</a>
 </li> --}}


</form>
        </div>
</div>


    @endsection



</body>
</html>
