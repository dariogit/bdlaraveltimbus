<?php

use App\Http\Controllers\EstudiantesController;
use App\Http\Controllers\TransportesController;
use App\Http\Controllers\InstitucionesController;


use Illuminate\Support\Facades\Route;
use Illuminate\Routing\Route as RoutingRoute;



use App\Models\instituciones;
use App\Models\transportes;


Route::get('/', function () {
    return view('welcome');
});

Route::get('/dashboard', function () {
    return view('dashboard');
})->middleware(['auth'])->name('dashboard');

require __DIR__.'/auth.php';


// Route::resource('estudiante', EstudiantesController::class);


// Route::get('/perfilEstudiante', function () {
//     return view('estudiante.perfilEstudiante');
// });


// Route::get('perfilEstudiante', [EstudiantesController::class, 'perfil']);

// Route::get('/perfilEstudiante', 'App\Http\Controllers\EstudiantesController@perfil');

// Route::view('dashboard', 'estudiante.index')->name('dashboard');


//USUARIO estudiante
Route::view('perfil', 'estudiante.perfilEstudiante')->name('perfil');

Route::view('rutaEstudiante', 'estudiante.rutaEstudiante')->name('rutaEstudiante');

Route::get('institucionEstudiante', 'App\Http\Controllers\UserController@institucionEstudiante')->name('institucionEstudiante');

Route::view('pagoTransporte', 'estudiante.pagoTransporte')->name('pagoTransporte');



//USUARIO institucion
Route::view('institucion', 'institucion.index')->name('institucion');



//USUARIO empresa
Route::view('empresa', 'empresa.index')->name('empresa');
Route::view('perfilEmpresa', 'empresa.perfil_empresa')->name('perfilEmpresa');
Route::view('rutaEmpresa', 'empresa.ruta_empresa')->name('rutaEmpresa');
// Route::view('institucionesVinculadas', 'empresa.inst_vinculadas')->name('institucionesVinculadas');
Route::get('institucionesVinculadas', 'App\Http\Controllers\TransportesController@institucionesVinculadas')->name('institucionesVinculadas');
Route::resource('/empresaInv', 'App\Http\Controllers\InstitucionesController');

Route::put('/eliminarEnv/{institucion}', [InstitucionesController::class, 'destroy'])->name('eliminarEnv.destroy');

Route::delete('/eliminarEnv/{institucion}', [InstitucionesController::class, 'destroy'])->name('eliminarEnv.destroy');

Route::get('editarEnv/{institucion}/edit', [InstitucionesController::class, 'edit'])->name('editarEnv.edit');

Route::put('/instUpdate/{institucion}', [InstitucionesController::class, 'update'])->name('inst.Update');

Route::get('vinInst/create', [InstitucionesController::class, 'create'])->name('vinInst.create');

Route::post('createInst/create', [InstitucionesController::class, 'store'])->name('createInst.store');


// Route::post('/empresaInv', 'App\Http\Controllers\TransportesController@store')->name('empresaInv.store');


Route::view('pagoInstituciones', 'empresa.pago_instituciones')->name('pagoInstituciones');
Route::view('registroVehiculo', 'empresa.reg_vehiculo')->name('registroVehiculo');







//USUARIO conductor
Route::view('conductor', 'conductor.index')->name('conductor');



Route::get('/vista1', function () {
    return view('estudiante.vista1');
});









