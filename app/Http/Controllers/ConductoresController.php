<?php

namespace App\Http\Controllers;

use App\Models\conductores;
use Illuminate\Http\Request;

class ConductoresController extends Controller
{

    public function index()
    {
        //
    }


    public function create()
    {
        //
    }


    public function store(Request $request)
    {
        //
    }


    public function show(conductores $conductores)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\conductores  $conductores
     * @return \Illuminate\Http\Response
     */
    public function edit(conductores $conductores)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\conductores  $conductores
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, conductores $conductores)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\conductores  $conductores
     * @return \Illuminate\Http\Response
     */
    public function destroy(conductores $conductores)
    {
        //
    }
}
