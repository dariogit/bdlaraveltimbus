<?php

namespace App\Http\Controllers;

use App\Models\registro_vehiculos;
use Illuminate\Http\Request;

class RegistroVehiculosController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\registro_vehiculos  $registro_vehiculos
     * @return \Illuminate\Http\Response
     */
    public function show(registro_vehiculos $registro_vehiculos)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\registro_vehiculos  $registro_vehiculos
     * @return \Illuminate\Http\Response
     */
    public function edit(registro_vehiculos $registro_vehiculos)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\registro_vehiculos  $registro_vehiculos
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, registro_vehiculos $registro_vehiculos)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\registro_vehiculos  $registro_vehiculos
     * @return \Illuminate\Http\Response
     */
    public function destroy(registro_vehiculos $registro_vehiculos)
    {
        //
    }
}
