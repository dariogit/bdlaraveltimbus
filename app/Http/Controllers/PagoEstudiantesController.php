<?php

namespace App\Http\Controllers;

use App\Models\pago_estudiantes;
use Illuminate\Http\Request;

class PagoEstudiantesController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\pago_estudiantes  $pago_estudiantes
     * @return \Illuminate\Http\Response
     */
    public function show(pago_estudiantes $pago_estudiantes)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\pago_estudiantes  $pago_estudiantes
     * @return \Illuminate\Http\Response
     */
    public function edit(pago_estudiantes $pago_estudiantes)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\pago_estudiantes  $pago_estudiantes
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, pago_estudiantes $pago_estudiantes)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\pago_estudiantes  $pago_estudiantes
     * @return \Illuminate\Http\Response
     */
    public function destroy(pago_estudiantes $pago_estudiantes)
    {
        //
    }
}
